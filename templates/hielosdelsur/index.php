<?php

/* ************************************************************************************************************************

Hielos del Sur

File:			index.php
Author:			Amapolazul Grupo Creativo
Homepage:		www.amapolazul.com
Copyright:		2016

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

JHtml::_( 'behavior.framework', true );

// Variables

$site_base = $_SERVER['HTTP_HOST']; // e.g. www.amapolazul.com
$site_path = 'http://' . $site_base; // e.g. http://www.amapolazul.com
$app = JFactory::getApplication();
$option = JRequest::getVar( 'option' );
$view = JRequest::getVar( 'view' );
$Itemid = JRequest::getVar( 'Itemid' );
$pageclass = $app->getParams( 'com_content' );
$pageclass_sfx = $pageclass->get( 'pageclass_sfx' );

// Template path

$path = 'templates/' . $this->template . '/';

// Modules

$show_banner = $this->countModules( 'banner' );
$show_bottom = $this->countModules( 'bottom' );
$show_client = $this->countModules( 'client' );
$show_map = $this->countModules( 'map' );
$show_menu = $this->countModules( 'menu' );
$show_product = $this->countModules( 'product' );
$show_top = $this->countModules( 'top' );

// Params

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
	<head>
		<jdoc:include type="head" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="google-site-verification" content="6zycSE_5avNBypmy9m4a_MgTDcy7HJL1xy-akH3TZcg">
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo $site_path; ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php echo $app->getCfg( 'sitename' ); ?>">
		<meta property="og:description" content="<?php echo $app->getCfg( 'MetaDesc' ); ?>">
		<meta property="og:image" content="<?php echo $site_path; ?>/<?php echo $path; ?>images/logo_ogp.png">
		<link rel="image_src" href="<?php echo $site_path; ?>/<?php echo $path; ?>images/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<link href="<?php echo $path; ?>js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>js/jquery/fancybox/jquery.fancybox.css?v=2.1.5" rel="stylesheet" type="text/css" media="screen">
		<link href="<?php echo $path; ?>css/template.css" rel="stylesheet" type="text/css">
		<link href="<?php echo $path; ?>assets/plugins/fontawesome/css/all.css" rel="stylesheet">
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Begin Google Analytics -->
		<script>

			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-81723806-1', 'auto');
			ga('send', 'pageview');

		</script>
		<!-- End Google Analytics -->
	</head>
	<body>
		<?php if ( $show_top ) : ?>
		<!-- Begin Top -->
			<div class="top_wrap">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="top" style="xhtml" />
					</div>
				</div>
			</div>
		<!-- End Top -->
		<?php endif; ?>
		<?php if ( $show_menu ) : ?>
		<!-- Begin Menu -->
			<div class="menu_wrap visible-xs">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
										<span class="sr-only">Menú</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
									<jdoc:include type="modules" name="menu" style="xhtml" />
								</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		<!-- End Menu -->
		<?php endif; ?>
		<?php if ( $show_banner ) : ?>
		<!-- Begin Banner -->
			<div class="banner_wrap<?php echo $pageclass_sfx; ?>">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<jdoc:include type="modules" name="banner" style="xhtml" />
						</div>
					</div>
				</div>
			</div>
		<!-- End Banner -->
		<?php endif; ?>
		<?php if ( $show_map ) : ?>
		<!-- Begin Map -->
			<div class="map_wrap">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-12">
							<jdoc:include type="modules" name="map" style="xhtml" />
						</div>
					</div>
				</div>
			</div>
		<!-- End Map -->
		<?php endif; ?>
		<!-- Begin Component -->
			<div class="component_wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<jdoc:include type="component" />
						</div>
					</div>
				</div>
			</div>
		<!-- End Component -->
		<?php if ( $show_product ) : ?>
		<!-- Begin Product -->
			<div class="product_wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<jdoc:include type="modules" name="product" style="xhtml" />
						</div>
					</div>
				</div>
			</div>
		<!-- End Product -->
		<?php endif; ?>
		<?php if ( $show_client ) : ?>
		<!-- Begin Client -->
			<div class="client_wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<jdoc:include type="modules" name="client" style="xhtml" />
						</div>
					</div>
				</div>
			</div>
		<!-- End Client -->
		<?php endif; ?>
		<?php if ( $show_bottom ) : ?>
		<!-- Begin Bottom -->
			<div class="bottom_wrap">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="bottom" style="xhtml" />
					</div>
				</div>
			</div>
		<!-- End Bottom -->
		<?php endif; ?>
		<!-- Begin Copyright -->
			<div class="copyright_wrap">
				<div class="copyright">
					&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo $this->baseurl; ?>"><?php echo $app->getCfg( 'sitename' ); ?></a>. Todos los derechos reservados. Sitio desarrollado por <a href="http://www.amapolazul.com" target="_blank">Amapola Azul</a>.
				</div>
			</div>
		<!-- End Copyright -->
		<!-- Begin WhatsApp -->
			<div class="whatsapp">
				<a href="https://api.whatsapp.com/send?phone=573118780758&amp;text=" target="_blank"><i class="fa-brands fa-whatsapp"></i></a>
			</div>
		<!-- End WhatsApp -->
		<!-- Begin Main Scripts -->
			<script src="<?php echo $path; ?>js/jquery/jquery.js"></script>
			<script src="<?php echo $path; ?>js/bootstrap/js/bootstrap.min.js"></script>
			<script src="<?php echo $path; ?>js/jquery/fancybox/jquery.fancybox.pack.js?v=2.1.5" type="text/javascript"></script>
			<script src="<?php echo $path; ?>js/jquery/scripts.js" type="text/javascript"></script>
		<!-- End Main Scripts -->
	</body>
</html>