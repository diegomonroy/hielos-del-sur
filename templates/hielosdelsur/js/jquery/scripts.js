// JavaScript Document

/* ************************************************************************************************************************

Hielos del Sur

File:			scripts.js
Author:			Amapolazul Grupo Creativo
Homepage:		www.amapolazul.com
Copyright:		2016

************************************************************************************************************************ */

jQuery.noConflict();

jQuery(document).ready(function() {
	// Cartera de Clientes
	jQuery( 'h3.special' ).append(
		'<br /><span>Contamos con una sólida e importante cartera de clientes, entre los cuales podemos mencionar a los siguientes:</span>'
	);
	// fancyBox
	jQuery( '#pedidos' ).fancybox({
		type: 'iframe'
	});
	jQuery( '.fancybox' ).fancybox();
});